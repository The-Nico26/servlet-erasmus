package Lab9;

import java.sql.*;
import java.util.TimeZone;

class Database {
    private Statement statement;
    private static Database db;

    private Database(){
        try {
            String url = "jdbc:mysql://localhost:3306/WWWBase1?serverTimezone=" + TimeZone.getDefault().getID();
            String username = "test";
            String password = "test";
            Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
            Connection conn = DriverManager.getConnection(url, username, password);
            this.statement = conn.createStatement();
        }catch (Exception ignored){}
    }

    private static Database getInstance(){
        if(db == null){
           db = new Database();
        }
        return db;
    }

    static ResultSet rowsSQL(String sql) throws SQLException{
        return getInstance().statement.executeQuery(sql);
    }
    static boolean executeSQL(String sql) throws SQLException {
        Database db = getInstance();
        return db.statement.execute(sql);
    }
}
