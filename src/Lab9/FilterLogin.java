package Lab9;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.sql.SQLException;

public class FilterLogin implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        try {
            HttpServletRequest httpServletRequest = (HttpServletRequest)servletRequest;
            String ip = httpServletRequest.getRemoteAddr();

            String action = httpServletRequest.getRequestURI();

            Database.executeSQL("INSERT INTO loghistory(ip, action) VALUES('"+ip+"', '"+action+"')");
            servletRequest.getRequestDispatcher("index.jsp").forward(servletRequest, servletResponse);

        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
    }

    @Override
    public void destroy() {

    }
}
