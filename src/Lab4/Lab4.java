package Lab4;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.HashMap;

public class Lab4 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        HttpSession hS = req.getSession(false);
        if(req.getSession(false) == null){
            resp.sendRedirect("/lab5");
            return;
        }else{
            if(!(boolean)hS.getAttribute("auth")){
                resp.sendRedirect("/lab5");
                return;
            }else{
                if(hS.getAttribute("ask") == null){
                    req.getRequestDispatcher("/Lab4/index.html").forward(req, resp);
                }
            }
        }
        String information = "<h2>Information:</h2>" +
                "Date: "+getValueCookie("datetime", req)+"<br>"+
                "Name: "+getValueCookie("name", req)+"<br>"+
                "Surname: "+getValueCookie("surname", req)+"<br>"+
                "Faculty: "+getValueCookie("Faculty", req)+"<br>";
        req.setAttribute("information", information);

        int askID = (int)hS.getAttribute("ask");
        String askPar = req.getParameter("ask");
        if(askPar != null){
            if(askPar.equalsIgnoreCase("reset")){
                AskHub.reset();
            }
            try {
                askID = Integer.parseInt(askPar);
                hS.setAttribute("ask", askID);
            }catch (Exception ignored){}
        }
        Ask a = AskHub.getAsk(askID);

        int answerAskID = -1;
        if(!(boolean)hS.getAttribute("newForm")){
            HashMap<Integer, Integer> answerForm = ((HashMap<Integer, Integer>) hS.getAttribute("answers"));
            answerAskID = answerForm.getOrDefault(askID, -1);
            if(a == null){
                String link = "";
                for (int index = 0; index < AskHub.length(); index++){
                    a = AskHub.getAsk(index);
                    link += "<a href=\"?ask="+index+"\">"+index+"</a> "+a.getQuestion()+":<br><div style=\"margin: 10px 0; border-bottom: 2px solid black; width: 100%;\">";
                    if(a.getAnswer() == answerForm.getOrDefault(index, -1)){
                        link += "<span class='correct'>OK</span>";
                    }else{
                        link += "<span class='incorrect'>NOT OK</span>";
                    }
                    link += " Correct answer: "+a.getChoices()[a.getAnswer()]+". Your answer: "+a.getChoices()[answerForm.get(index)]+"</div><br>";
                }
                req.setAttribute("link", link);
                hS.setAttribute("result", true);
                req.setAttribute("result", AskHub.result(answerForm));
                req.setAttribute("total", AskHub.length());
                req.getRequestDispatcher("/Lab4/final.jsp").forward(req, resp);
                return;
            }
        }

        String link = "";
        for (int index = 0; index < AskHub.length(); index++){
            link += "<a href=\"?ask="+index+"\">"+index+"</a>";
        }
        req.setAttribute("link", link);
        String choices = "";
        boolean finish = (boolean)hS.getAttribute("result");
        for (int index = 0; index < a.getChoices().length; index++){
            choices += "<input type=\"radio\" name=\"choices\" value=\""+index+"\" "+(answerAskID == index ? "checked":"")+" "+(finish?"disabled":"")+"><span class=\""+(finish ? (index== a.getAnswer()?"correct": (answerAskID == index ? "incorrect":"") ) :"")+"\">"+a.getChoices()[index]+"</span><br>";
        }

        req.setAttribute("choices", choices);
        req.setAttribute("ask", "#"+askID+" "+a.getQuestion());
        req.getRequestDispatcher("/Lab4/ask.jsp").forward(req, resp);
    }

    private String getValueCookie(String name, HttpServletRequest request){
        Cookie[] cookies = request.getCookies();
        for (Cookie cookie : cookies){
            if(cookie.getName().equalsIgnoreCase(name)){
                return cookie.getValue();
            }
        }
        return "";
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        // Information date, name, surname and faculty
        Cookie datetime = new Cookie("datetime", req.getParameter("date"));
        resp.addCookie(datetime);
        Cookie name = new Cookie("name", req.getParameter("name"));
        resp.addCookie(name);
        Cookie surname = new Cookie("surname", req.getParameter("surname"));
        resp.addCookie(surname);
        Cookie faculty = new Cookie("faculty", req.getParameter("faculty"));
        resp.addCookie(faculty);

        HttpSession hS = req.getSession();

        hS.setAttribute("ask", 0);
        hS.setAttribute("newForm", true);
        HashMap<Integer, Integer> answerForm = new HashMap<>();
        hS.setAttribute("answers", answerForm);
        hS.setAttribute("result", false);

        resp.sendRedirect("/lab4");
    }
}
