package Lab4;

import java.util.ArrayList;
import java.util.HashMap;

class AskHub {
    private ArrayList<Ask> asks;
    private static AskHub askHub;

    private AskHub(){
        asks = new ArrayList<Ask>();
        init();
    }

    private void init(){
        asks.add(new Ask("Send 1 ?", 1, "0", "1", "2", "3"));
        asks.add(new Ask("Send 'two' ?", 2, "zero", "one", "two", "three"));
//        asks.add(new Ask("Faculty ?", 2, "Bio", "Info", "Social", "Science"));
//        asks.add(new Ask("Response to life ?", 4, "44", "41", "43", "42"));
    }

    private static AskHub getInstance(){
        if(askHub == null){
            askHub = new AskHub();
        }
        return askHub;
    }
    static Ask getAsk(int id){
        if(id >= getInstance().asks.size()) return null;
        return getInstance().asks.get(id);
    }
    static Integer length(){
        return getInstance().asks.size();
    }
    static void reset(){
        askHub = new AskHub();
    }

    static int result(HashMap<Integer, Integer> answerForm){
        int result = 0;
        for(int index = 0; index < AskHub.length(); index++){
            if(AskHub.getAsk(index).getAnswer() == answerForm.getOrDefault(index, -1)){
                result++;
            }
        }
        return result;
    }
}