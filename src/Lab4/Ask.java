package Lab4;

public class Ask {
    private String question;
    private String choices[];
    private int answer;

    public Ask(String question, int answer, String ... choices) {
        this.question = question;
        this.choices = choices;
        this.answer = answer;
    }

    public int getAnswer() {
        return answer;
    }

    public String getQuestion() {
        return question;
    }

    public String[] getChoices() {
        return choices;
    }

    public boolean checkAnswer(int answer){
        return answer == this.answer;
    }
}
