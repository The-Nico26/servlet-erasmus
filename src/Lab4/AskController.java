package Lab4;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;

public class AskController extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession hS = req.getSession(false);
        int askID = (int) hS.getAttribute("ask");
        try {
            int answer = Integer.parseInt(req.getParameter("choices"));
            HashMap<Integer, Integer> answerForm = (HashMap<Integer, Integer>) hS.getAttribute("answers");
            if (answerForm.containsKey(askID)) {
                answerForm.replace(askID, answer);
            } else {
                answerForm.put(askID, answer);
            }
        }catch (Exception ignored){}
        hS.setAttribute("newForm", false);
        hS.setAttribute("ask", askID+1);

        resp.sendRedirect("/lab4");
    }
}
