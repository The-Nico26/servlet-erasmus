package Lab8;

public class ValidateBean {
    private String username;
    private String password;
    private String age;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public boolean validate(String usn, String pass){
        return usn.equals(getUsername()) && pass.equals(getPassword());
    }
}
