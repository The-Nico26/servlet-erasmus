package Lab5;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;

public class googleSearch extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        String html = "<form action='/googleSearch' method='POST'>" +
                "<input type='text' name='s' placeholder='Search'><br>" +
                "<input type='submit' value='Search'>" +
                "</form>";
        try(PrintWriter writer = resp.getWriter()) {
            writer.println(html);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String search = req.getParameter("s");
        resp.sendRedirect("https://www.google.com/search?q="+search);
    }
}
