package Lab5;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class Lab5 extends HttpServlet{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/Lab5/index.html").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String user = req.getParameter("user");
        String password = req.getParameter("password");
        HttpSession hS = req.getSession();
        if(user.equals("demo") && password.equals("demo")){
            hS.setAttribute("auth", true);
            resp.sendRedirect("/Lab4");
        }else{
            hS.setAttribute("auth", false);
            req.getRequestDispatcher("/Lab5/error.html").include(req, resp);
        }
    }
}
