package Lab3;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class Servlet extends HttpServlet {

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        resp.setContentType("text/html");
        String id = req.getParameter("id");
        try (PrintWriter printWriter = resp.getWriter()) {
            printWriter.println("<h2>Works !"+id+"</h2>");
        } catch (Exception ignored) {}
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        String name = req.getParameter("username");
        String age = req.getParameter("userage");
        String gender = req.getParameter("gender");
        String faculty = req.getParameter("faculty");
        String[] courses = req.getParameterValues("courses");

        try(PrintWriter writer = resp.getWriter()){
            writer.println("Your name: "+name+"<br>");
            writer.println("Your age: "+age+"<br>");
            writer.println("Your gender: "+gender+"<br>");
            writer.println("Your faculty: "+faculty+"<br>");
            writer.println("Your courses: ");
            for(String course : courses){
                writer.println("<li>"+course+"</li>");
            }
        }catch (Exception ignored){}
    }
}
