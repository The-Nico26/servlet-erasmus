package Lab7;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.TimeZone;

@WebServlet("/Lab7")
public class Lab7 extends HttpServlet {

    public Lab7(){
        super();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter writer = resp.getWriter();
        try{
            String url = "jdbc:mysql://localhost:3306/WWWBase1?serverTimezone="+ TimeZone.getDefault().getID();
            String username = "test";
            String password = "test";
            Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
            Connection conn = DriverManager.getConnection(url, username, password);
            writer.println("Connection OK");
            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM books");
            writer.println("<table border='1'>");
            while(resultSet.next()){
                writer.println("<tr>");
                writer.println("<td>"+resultSet.getInt("id")+"</td>");
                writer.println("<td>"+resultSet.getString("name")+"</td>");
                writer.println("<td>"+resultSet.getString("price")+"</td>");
                writer.println("</tr>");
            }
            writer.println("</table>");
        }catch (Exception ex){
            writer.println("Connection FAILER");
            writer.println(ex);
        }finally {
            writer.close();
        }
    }
}
