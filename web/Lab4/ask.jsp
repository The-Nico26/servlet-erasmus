<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Form</title>
        <link rel="stylesheet" href="/Lab4/design.css">

    </head>
    <body>
        ${information}<br><br>
        <div class="link">
            Ask: ${link}<br>
        </div>

        <form method="POST" action="/lab4/formAsk">
            <h4>${ask}</h4>
            ${choices}<br>
            <input type="submit" value="Submit">
        </form>
    </body>
</html>