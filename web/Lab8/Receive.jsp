<%--
  Created by IntelliJ IDEA.
  User: Nico
  Date: 05/04/2019
  Time: 10:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Action LogIn</title>
    </head>
    <body>
        <jsp:useBean id="snr" class="Lab8.ValidateBean"/>
        <jsp:setProperty name="snr" property="*"/>
        You entered username as <b><jsp:getProperty name="snr" property="username"/></b><br>
        You entered password as <b><jsp:getProperty name="snr" property="password"/></b><br>
        You entered age as <b><jsp:getProperty name="snr" property="age"/></b><br>
        <br>
        <% if(snr.validate("test", "test")){
            out.println("Successful identification, Good Day !");
        }else{
            out.println("Error identification");
        }%><br>
        <b>Thank you</b>
    </body>
</html>
