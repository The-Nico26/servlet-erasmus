<%@ page import="java.util.Date" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="java.net.URLDecoder" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    HttpSession hS = request.getSession();
    String name = "", gender = "", age = "", country = "", coursesL = "", lastDate;
    if(request.getParameter("name") == null){
        for (Cookie c : request.getCookies()){
            if(c.getName().equals("name"))
                name = c.getValue();
            else if(c.getName().equals("gender"))
                gender = c.getValue();
            else if(c.getName().equals("age"))
                age = c.getValue();
            else if(c.getName().equals("country"))
                country = c.getValue();
            else if(c.getName().equals("courses"))
                coursesL = c.getValue();
        }
    }else{
        name = request.getParameter("name");
        gender = request.getParameter("gender");
        age = request.getParameter("age");
        country = request.getParameter("country");
        for (String course : request.getParameterValues("courses"))
            coursesL += URLEncoder.encode("<li>"+course+"</li>", "UTF-8");

        Cookie nC = new Cookie("name", name);
        response.addCookie(nC);
        Cookie gC = new Cookie("gender", gender);
        response.addCookie(gC);
        Cookie aC = new Cookie("age", age);
        response.addCookie(aC);
        Cookie cC = new Cookie("country", country);
        response.addCookie(cC);
        Cookie t = new Cookie("courses", coursesL);
        response.addCookie(t);
    }
    if(hS.getAttribute("lastDate") == null)
        lastDate = new Date().toString();
    else
        lastDate = (String)hS.getAttribute("lastDate");
%>

<html>
<head>
    <title>Hello <%= name %></title>
</head>
<body>
    <b>Visit time:</b> <%= new Date().toString() %><br>
    <b>Last visit time:</b> <%= lastDate %><br><br>

    <b>Your name:</b> <%= name %><br>
    <b>Gender:</b> <%= gender %><br>
    <b>Age:</b> <%= age %><br>
    <b>Country:</b> <%= country %><br>
    <br>
    <b>Courses:</b><br>
    <ul>
        <%= URLDecoder.decode(coursesL, "UTF-8") %>
    </ul>
    <%
        hS.setAttribute("lastDate", new Date().toString());
    %>
</body>
</html>
